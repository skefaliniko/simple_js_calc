
document.addEventListener("DOMContentLoaded", function() {

	var currentExpression = '';

	document.getElementById("calculator").addEventListener("click", function(ev) {

		if (ev.target.id != "display" && ev.target.id != "calculator") {
			
			var currentCharacter = '' + ev.target.innerHTML;

			switch(currentCharacter) {
			case '=':
				display.innerHTML = '' + evaluateExpression(currentExpression);
				currentExpression = '';
				break;
			case '←':
				display.innerHTML = '' + clearLastCharacter();
                currentExpression = '' + display.innerHTML;
				break;
			default:
				currentExpression += '' + ev.target.innerHTML;
				display.innerHTML = currentExpression;
				break;
			}
		}
		
	});
})


function evaluateExpression(exp) {
	
	let dotSequenceRegExp = /\d(\.){2,}\d/g;						//'586.....5676'
	let operatorsSequenceRegExp = /\d[-+*/]{2,}\d/g;				//'6..57+/*-2845'
	let mixedDotAndOperatorsSequenceRegExp = /\.[-+*/]|[-+*/]\./g;	//'6.*57-.28+.98790.89-.45.*.548'
	let zeroSequenceRegExp = /[-+*/]0{2,}(\.0{1,})?[-+*/.]/g;		//'-0000+452*0000.0000000+45.000+5-00.0+45+0.000045'
	let manyDotsInNumberRegExp = /\d{1,}(\.\d{1,}){2,}/g;			//'45.65.4/6.65.345+4674.32.3'
	let bracketsAndOtherRegExp = /\.\(|\.\)|\)\.|\(\.|\d\(|\)\d/g;	//'67.(76586+5615.)+45-(61+5915).65/45(.4515+592)-(2622+261)66'
	let divisionByZeroRegExp = /\d{1,}\/0(\.0{1,})?/g;				//'58965/0+656/0.0-6256/0.000000'

	var result = 'ERR';

	try {

		if (dotSequenceRegExp.test(exp)) {
			alert('Crazy decimal sequence between digits');
		} else if (operatorsSequenceRegExp.test(exp)) {
			alert('Crazy operator signs sequence between digits');
		} else if (mixedDotAndOperatorsSequenceRegExp.test(exp)) {
			alert('Smth wrong with dot\'s position - sequence mixed with dots and operators');
		} else if (zeroSequenceRegExp.test(exp)) {
			alert('Smth wrong with zeros sequence');
		} else if (manyDotsInNumberRegExp.test(exp)) {
			alert('Smth wrong with number record - there are a lot of dots in a number');
		} else if (bracketsAndOtherRegExp.test(exp)) {
			alert('Smth wrong with brackets and dots or digits');
		} else if (divisionByZeroRegExp.test(exp)) {
			alert('You try to divide by zero! It may cause the collapse of the universe!')
		} else {
			result = '' + recursiveEvaluation(exp);
		}

		if (result === undefined) {
			result = 'ERR';
		}

	} catch(e) {

		alert(e);
		result = 'ERR';

	}

	return result;
}


function clearLastCharacter() {

	var result = '';

	if (display.innerHTML === 'ERR') {
		result = '0';
	} else {
		result = '' + display.innerHTML.substring(0, display.innerHTML.length - 1);
	}

	return result;
}


function recursiveEvaluation(exp) {
    var innerBracketsRegExp = /\([0-9.]{1,}([-+*/][0-9.]*)*\)/;
    var negative = /-[0-9.]*/;
    
    var brackets = exp.match(innerBracketsRegExp);
    var result = '';

    if (brackets == null) {

        result = eval(exp);

    } else {

        var result_in_brackets = '' + eval(brackets[0]);
        var result_in_brackets_match = result_in_brackets.match(negative);

        if(result_in_brackets_match != null) {
            result_in_brackets = exp.replace(innerBracketsRegExp, "\("+result_in_brackets+"\)");
        } else {
        result_in_brackets = exp.replace(innerBracketsRegExp, result_in_brackets);
        }

        result = recursiveEvaluation(result_in_brackets);
    }

    return result;
}
